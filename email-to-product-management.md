Dear Product Management team,


I'm writing to you because of the Importer feature we're about to build. First of all, the team would like to thank you for the requirements gathering and the detail in the specs.

Still, the team has some concerns and the feeling that not every aspect is fully documented, for example, what should happen when:
- the user tries to upload a very big file?
- the upload takes so long the user gets distracted with something else?
- the user uploads a file, does the mapping and then uploads another file and proceeds to map it too, should we delete all the existing relations or build upon it? 

Also, about functionality:
- Should we keep objects which have no related fields? 
- What about fields with no objects?


Looking forward to hearing from you,
Development Team. 