<?php

/**
 * IObject since Object is a reserved keyword.
 */
class IObject {
  /** @var string */
  private $id;

  /** @var int */
  private $categoryId;

  /** @var int */
  private $sectorId;

  /** @var string */
  private $oid;

  /** @var float */
  private $latitude;

  /** @var float */
  private $longitude;

  // Usually managed by the ORM framework...
  // private $updatedAt;
  // private $createdAt;

  /**
   * @return string
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @return IObject
   */
  public function setId($id) {
    $this->id = $id;

    return $this;
  }

  /**
   * @return int
   */
  public function getCategoryId() {
    return $this->categoryId;
  }

  /**
   * @return IObject
   */
  public function setCategoryId($categoryId) {
    $this->categoryId = $categoryId;

    return $this;
  }

  // The following setters / getters
  // ...
}