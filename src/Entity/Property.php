<?php

class Property {
  /** @var string */
  private $name;

  /** @var string */
  private $dataType;

  /** @var string */
  private $options;

  // Usually managed by the ORM framework...
  // private $updatedAt;
  // private $createdAt;

  /**
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @return Property
   */
  public function setName($name) {
    $this->name = $name;

    return $this;
  }

  // The following setters / getters
  // ...
}