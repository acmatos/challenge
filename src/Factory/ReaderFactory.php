<?php

require ('./../Reader/XLSXReader.php');
require ('./../Reader/CSVReader.php');

class ReaderFactory
{

  const FILE_TYPE_XLSX = 'xlsx';
  const FILE_TYPE_CSV = 'csv';

  /**
   * @property string $file
   */
  private $file; 

  public function setFile($file) 
  {
    $this->file = $file;

    return $this;
  }

  public function getReader()
  {
    // Based on file type we return the right importer
    $fileType = 'filetype';

    // /!\ Assuming file type was validated at the upload 
    // and no wrong types can be found here.
    return self::FILE_TYPE_XLSX === $fileType 
      ? new XLSXReader($this->file)
      : new CSVReader($this->file);
  }
}