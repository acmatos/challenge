<?php

require ('./../Entity/IObject.php');
require ('./../Entity/Property.php');

class XLSXReader 
{
  private property $file;

  public function __construct($file)
  {
    $this->file = $file;
  }

  /**
   * This is about reading the $file's contents and 
   * organizing it. Then the customer will be able to 
   * match IObject and Property to his liking.
   * 
   * @return array[IObject, Property] $objectPropertyMap;
   */
  public function read()
  {
    // /!\ skipping implementation 
    // I expect the result of this implementation 
    // to return something like the following.

    $objectPropertyMap = [
      'IObject' => [
        new IObject(),
        // ...
      ], 
      'Property' => [
        new Property(),
        // ...
      ]
    ];

    return $objectPropertyMap;
  }
}