<?php

// For this API, I'd create three endpoints:
// ----------------------------------------------------
// POST /uploadImportFile/{idCustomer} -- uploadimportFile($idCustomer, $path)
// GET  /mapObjectProperty/{idCustomer} -- getMapObjectProperty($idCustomer)
// POST /mapObjectProperty/{idCustomer} -- setMapObjectProperty($idCustomer, $objectPropertyMap)
// ----------------------------------------------------
// Assuming this file is the entry point for our application and
// disregarding autoloaders, routes, dependency injection and/or any 
// other goodies that are usually available on a framework such as symfony.


// /!\ I'll do some assumptions across this exercise marked as /!\


require ('./Strategy/ImporterStrategy.php');

class FakeAPI 
{
  /**
   * Manage the file uploaded by the customer
   * and store it in a recoverable way.
   * 
   * /!\ I'm assuming the Object and Properties are not transversal
   * to all customers. Therefore I'll have a customer ID to identify
   * the customer's file and the customer will be able to map whenever 
   * he wants.
   * 
   * @param int $idCustomer
   * @param string $path 
   */
  public function uploadImportFile($idCustomer, $path)
  {
    try {
      // basic validation on the file and store it somewhere. 
      // It could be /object-property/id-customer.filetype

      return true;
    } catch (\Exception $e) {
      throw new Exception($e->getMessage());
    }
  }

  /**
   * Find customer's uploaded file based on ID 
   * 
   * @param int $idCustomer
   */
  public function getMapObjectProperty($idCustomer)
  {
    $file = '/object-property/id-customer.filetype';

    // We need to read the file, it can come in two flavours
    // and we must figure out which one we're dealing with.
    $factory = new ReaderFactory();
    $mapper = $factory->setFile($file)->getReader();

    return $mapper->read();
  }

  /**
   * Store information posted by the customer.
   * 
   * /!\ I'm assuming not all Objects and not all Properties are 
   * going to be imported. Therefore I only pretend to save it after 
   * the mapping is done. This way I can discard all Objects/Properties 
   * that are not to be mapped.
   * 
   * @param array $objectPropertyMap
   */
  public function setMapObjectProperty($idCustomer, $objectPropertyMap)
  {
    // /!\ skipping implementation that would likely live in 
    // another place, together with storing the info into the db.
    // I expect $objectPropertyMap to look something like
    $objectPropertyMap = [
      'IObject' => [
        'id' => '0af00e43-a12f-410e-8721-2e650d61e21f',
        'categoryId' => '...',
        // ...
      ],
      'Property' => [
        'id' => '9b84c523-2e27-4193-9c87-42b872db7667',
        'name' => '...',
        // ...
      ],
      'map' => [
        [
          'objectId' => '0af00e43-a12f-410e-8721-2e650d61e21f',
          'propertyId' => '9b84c523-2e27-4193-9c87-42b872db7667',
          'value' => '...',
        ],
        // ...
      ],
    ];

    // finally, the result of the operation is sent back to the customer
    $count = 123;
    return [
      'errors' => [],
      'summary' => sprintf('Success! %d objects were just imported.', $count),
    ];
  }
}
